import module from 'node:module';

import { describe, expect, test } from 'vitest';

import formatter from '../src/index.js';

const require = module.createRequire(import.meta.url);

describe('formatter', () => {
  test('empty results', () => expect(formatter([])).toEqual('[]'));

  [
    'basic-data',
    'parse-error',
  ].forEach((name) => {
    test(`fixture run: ${name}`, () => {
      const data = require(`./fixtures/${name}.json`);
      const results = formatter(data);

      expect(typeof results).toEqual('string');
      expect(JSON.parse(results)).toMatchSnapshot();
    });
  });
});
