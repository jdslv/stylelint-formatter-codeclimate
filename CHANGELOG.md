# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.1.0] - 2025-02-15

### Changed
- Migrate to ESM
- Rewrite in TS
- Migrate to `pnpm`
- Migrate to `vitest`


## [1.0.0] - 2020-10-25

First version
