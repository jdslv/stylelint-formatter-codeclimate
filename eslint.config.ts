import javascript from '@eslint/js';
import typescript from 'typescript-eslint';

export default [
  {
    ignores: [
      'lib/',
    ],
  },
  javascript.configs.recommended,
  ...typescript.configs.recommended,
];
