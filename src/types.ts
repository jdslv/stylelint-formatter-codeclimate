export type CodeClimateSeverity = 'blocker' | 'critical' | 'info' | 'major' | 'minor';
export type StylelintSeverity = 'error' | 'warning';

export type CodeClimateCategorie =
  | 'Bug Risk'
  | 'Clarity'
  | 'Compatibility'
  | 'Complexity'
  | 'Duplication'
  | 'Performance'
  | 'Security'
  | 'Style';

interface LocationLines {
  lines: Record<'begin' | 'end', number>;
  path: string;
}

type Position = Record<'column' | 'line', number>;

interface LocationPositions {
  path: string;
  positions: Record<'begin' | 'end', Position>;
}

type Location = LocationLines | LocationPositions;

export interface CodeClimateResult {
  categories: CodeClimateCategorie[];
  check_name: string;
  content?: string;
  description: string;
  fingerprint?: string;
  location: Location;
  other_locations?: Location[];
  remediation_points?: number;
  severity?: CodeClimateSeverity;
  type: 'issue';
}

export interface StylelintParseError {
  column: number;
  line: number;
  rule: string;
  severity: StylelintSeverity;
  text: string;
}

export interface StylelintWarning {
  column: number;
  line: number;
  rule: string;
  severity: StylelintSeverity;
  text: string;
}

export interface StylelintResult {
  deprecations: unknown[];
  errored: boolean;
  invalidOptionWarnings: unknown[];
  ignored: boolean;
  parseErrors: StylelintParseError[];
  source: string;
  warnings: StylelintWarning[];
}
