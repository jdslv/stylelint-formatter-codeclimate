import { createHash } from 'node:crypto';

import type { CodeClimateResult, CodeClimateSeverity, StylelintResult, StylelintSeverity } from './types.js';

const severityMap: Record<StylelintSeverity, CodeClimateSeverity> = {
  error: 'major',
  warning: 'minor',
};

const createItem = (
  path: string,
  line: number,
  column: number,
  rule: string,
  description: string,
  severity: CodeClimateSeverity,
): CodeClimateResult => {
  const parts = [
    path,
    line,
    column,
    rule,
  ];

  return {
    categories: [
      'Style',
    ],
    check_name: rule,
    description,
    fingerprint: createHash('sha256').update(parts.join('::')).digest('hex'),
    location: {
      lines: {
        begin: line,
        end: line,
      },
      path,
    },
    severity,
    type: 'issue',
  };
};

export const formatter = (results: StylelintResult[]): string => {
  const items: CodeClimateResult[] = [];

  results.forEach(({source, warnings, parseErrors}) => {
    if (warnings) {
      warnings.forEach(({line, column, rule, text, severity}) => {
        items.push(createItem(source, line, column, rule, text, severityMap[severity]));
      });
    }

    if (parseErrors) {
      parseErrors.forEach(({line, column, text}) => {
        items.push(createItem(source, line, column, 'parse-error', text, 'blocker'));
      });
    }
  });

  return JSON.stringify(items);
};
export default formatter;
